const hamburgerBtn = document.getElementById('hamburger-btn'); 
const crossBtn = document.getElementById('cross-btn'); 
const mainEl = document.querySelector('main'); 
const mobileNavEl = document.querySelector('.mobile-nav'); 
const navigationMenuEl = document.querySelector('.navigation-menu'); 

hamburgerBtn.addEventListener('click', () => {
    mainEl.style.display = 'none'; 
    mobileNavEl.style.display = 'none'; 
    navigationMenuEl.style.display = 'flex'; 
}); 

crossBtn.addEventListener('click', () => {
    mainEl.style.display = "flex";
    mobileNavEl.style.display = "flex";
    navigationMenuEl.style.display = "none"; 
});     
