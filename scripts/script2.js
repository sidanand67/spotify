// signup.html elements
const fnameEl = document.getElementById("fname");
const lnameEl = document.getElementById("lname");
const emailEl = document.getElementById("email");
const passwdEl = document.getElementById("passwd");
const cpasswdEl = document.getElementById("confirm-passwd");
const termsEl = document.getElementById("terms-btn");
const signUpButton = document.getElementById("sign-up-btn"); 
const signUpFormEl = document.querySelector('.signup-form'); 
const successBanner = document.querySelector('.success'); 
const lenInfoEl = document.getElementById('len-info'); 
const alphaInfoEl = document.getElementById('alpha-info'); 
const digitInfoEl = document.getElementById('digit-info'); 
const splInfoEl = document.getElementById('spl-info'); 
const formEl = document.getElementsByTagName('form'); 

// signup.html elements errors
const fnameErrorEl = document.getElementById("fname-error");
const lnameErrorEl = document.getElementById("lname-error");
const emailErrorEl = document.getElementById("email-error");
// const passwdErrorEl = document.getElementById("passwd-error");
const cpasswdErrorEl = document.getElementById("cpasswd-error");
const termsErrorEl = document.getElementById("terms-error"); 

// signup.html validations 
function fnameValidation(){
    let fname = fnameEl.value.trim(); 
    let namePattern = /^[a-zA-Z]+$/; 

    if (fname.length === 0) {
        fnameErrorEl.innerText = "Please enter your first name.";
        fnameEl.classList.add("invalid-tag");
        fnameErrorEl.classList.add("invalid-msg");
        return 1; 
    } 
    else if(fname.includes(' ')){
        fnameErrorEl.innerText = "Spaces are not allowed in first name.";
        fnameEl.classList.add("invalid-tag");
        fnameErrorEl.classList.add("invalid-msg");
        return 1; 
    }
    else if(namePattern.test(fname) === false){
        fnameErrorEl.innerText = "Only alphabets are allowed.";
        fnameEl.classList.add("invalid-tag");
        fnameErrorEl.classList.add("invalid-msg");
        return 1; 
    }
    else {
        fnameEl.classList.remove("invalid-tag");
        fnameErrorEl.classList.remove("invalid-msg");
        return 0; 
    }
}

function lnameValidation(){
    let lname = lnameEl.value.trim(); 
    let namePattern = /^[a-zA-Z]+$/; 

    if (lname.length === 0) {
        lnameErrorEl.innerText = "Please enter your last name.";
        lnameEl.classList.add("invalid-tag");
        lnameErrorEl.classList.add("invalid-msg");
        return 1; 
    } 
    else if(lname.includes(' ')){
        lnameErrorEl.innerText = "Spaces are not allowed in last name.";
        lnameEl.classList.add("invalid-tag");
        lnameErrorEl.classList.add("invalid-msg");
        return 1;
    }
    else if(namePattern.test(lname) === false){
        lnameErrorEl.innerText = "Only alphabets are allowed.";
        lnameEl.classList.add("invalid-tag");
        lnameErrorEl.classList.add("invalid-msg");
        return 1; 
    }
    else {
        lnameEl.classList.remove("invalid-tag");
        lnameErrorEl.classList.remove("invalid-msg");
        return 0; 
    }
}

function emailValidation(){
    let email = emailEl.value.trim(); 
    let emailPattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 

    if (email.length === 0) {
        emailErrorEl.innerText = "Please enter your email.";
        emailEl.classList.add("invalid-tag");
        emailErrorEl.classList.add("invalid-msg");
        return 1; 
    } else if (emailPattern.test(email) === false) {
        emailErrorEl.innerText = "Please enter a valid email.";
        emailEl.classList.add("invalid-tag");
        emailErrorEl.classList.add("invalid-msg");
        return 1; 
    } else {
        emailEl.classList.remove("invalid-tag");
        emailErrorEl.classList.remove("invalid-msg");
        return 0; 
    }
}

function passwordValidation(){
    let password = passwdEl.value.trim(); 

    let alphaPattern = /[a-zA-Z]/; 
    let digitPattern = /[0-9]/; 
    let specialPattern = /[!@#$%^&*]/; 

    let errorObj = {
        len: false,
        alpha: false, 
        digit: false, 
        special: false
    }

    if(password.length === 0){
        passwdEl.classList.add("invalid-tag");
        lenInfoEl.style.color = "red";
        alphaInfoEl.style.color = "red";
        digitInfoEl.style.color = "red";
        splInfoEl.style.color = "red"; 
    }

    if(password.length < 8){
        lenInfoEl.style.color = "red"; 
        errorObj.len = false; 
    }
    else {
        lenInfoEl.style.color = "green"; 
        errorObj.len = true;
    }

    if (alphaPattern.test(password) === false){
        alphaInfoEl.style.color = "red"; 
        errorObj.alpha = false; 
    }
    else {
        alphaInfoEl.style.color = "green"; 
        errorObj.alpha = true; 
    }

    if (digitPattern.test(password) === false){
        digitInfoEl.style.color = "red"; 
        errorObj.digit = false; 

    }
    else {
        digitInfoEl.style.color = "green"; 
        errorObj.digit = true; 
    }

    if(specialPattern.test(password) === false){
        splInfoEl.style.color = "red"; 
        errorObj.special = false; 
    }
    else {
        splInfoEl.style.color = "green"; 
        errorObj.special = true; 
    }
    
    for (let key in errorObj){
        if(errorObj[key] === false){
            passwdEl.classList.add("invalid-tag");
            return 1; 
        }
    }

    passwdEl.classList.remove("invalid-tag");
    return 0; 
}

function cPasswordValidation(){
    let confirmPass = cpasswdEl.value.trim(); 

    if (confirmPass.length === 0) {
        cpasswdErrorEl.innerText = "You need to confirm your password.";
        cpasswdEl.classList.add("invalid-tag");
        cpasswdErrorEl.classList.add("invalid-msg");
        return 1; 
    } else if (cpasswdEl.value !== passwdEl.value) {
        cpasswdErrorEl.innerText = "Passwords doesn't match.";
        cpasswdEl.classList.add("invalid-tag");
        cpasswdErrorEl.classList.add("invalid-msg");
        return 1; 
    } else {
        cpasswdEl.classList.remove("invalid-tag");
        cpasswdErrorEl.classList.remove("invalid-msg");
        return 0; 
    }
}

function termsValidation(){
    if (termsEl.checked === false) {
        termsErrorEl.textContent = "Please accept the terms.";
        termsErrorEl.classList.add("invalid-msg");
        return 1; 
    } else {
        termsErrorEl.classList.remove("invalid-msg");
        return 0; 
    }
}

fnameEl.addEventListener('focusout', fnameValidation); 
fnameEl.addEventListener('input', fnameValidation); 

lnameEl.addEventListener("focusout", lnameValidation); 
lnameEl.addEventListener("input", lnameValidation); 

emailEl.addEventListener("focusout", emailValidation); 
emailEl.addEventListener("input", emailValidation); 

passwdEl.addEventListener('focusout', passwordValidation); 
passwdEl.addEventListener('input', passwordValidation);

cpasswdEl.addEventListener("focusout", cPasswordValidation); 
cpasswdEl.addEventListener("input", cPasswordValidation); 

termsEl.addEventListener("focusout", termsValidation); 

signUpButton.addEventListener("click", () => {
    if ((fnameValidation() + lnameValidation() + emailValidation() + passwordValidation() + cPasswordValidation() + termsValidation()) === 0){
        sessionStorage.setItem('name', fnameEl.value.trim()); 
        formEl[0].submit(); 
    } 
});     