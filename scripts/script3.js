let containerEl = document.querySelector('.container'); 
let userGreetingEl = document.querySelector('.user-greeting'); 

function createElement(product){
    let prodDiv = document.createElement('div'); 
    prodDiv.classList.add('product');
    containerEl.append(prodDiv); 

    let prodImgDiv = document.createElement('div'); 

    let prodImg = document.createElement('img');
    prodImg.classList.add('product-img');
    prodImg.src = product.image; 
    prodImg.alt = "product-img"; 
    prodImgDiv.append(prodImg);  
    prodDiv.append(prodImgDiv); 
    
    let prodDescDiv = document.createElement('div'); 
    prodDescDiv.classList.add('product-desc'); 
    prodDiv.append(prodDescDiv);

    let prodCategory = document.createElement('p'); 
    prodCategory.classList.add('category'); 
    prodCategory.textContent = product.category; 
    prodDescDiv.append(prodCategory); 

    let prodTitle = document.createElement('p');
    prodTitle.classList.add('title');  
    prodTitle.textContent = `${product.title.slice(0,50)}`; 
    prodDescDiv.append(prodTitle); 


    let prodDesc = document.createElement('p'); 
    prodDesc.classList.add('description'); 
    prodDesc.textContent = `${product.description.slice(0,120)}...`; 
    prodDescDiv.append(prodDesc); 

    let prodRating = document.createElement('p');
    prodRating.classList.add('rating'); 
    prodRating.innerHTML = `<i class="fa-solid fa-star rating-icon"></i>${product.rating.rate}`;  
    prodDescDiv.append(prodRating); 

    let prodRatingCount = document.createElement('span');
    prodRatingCount.classList.add('count');  
    prodRatingCount.textContent = ` (${product.rating.count} Users Rated)`; 
    prodRating.append(prodRatingCount); 

    let prodPrice = document.createElement('p');
    prodPrice.classList.add('price');  
    prodPrice.innerHTML = `<i class="fa-solid fa-dollar-sign dollar"></i>${product.price.toFixed(2)}`; 
    prodDescDiv.append(prodPrice); 

}


async function fetchProducts(){
    const loaderDiv = document.createElement('div'); 
    loaderDiv.classList.add('loader'); 
    containerEl.append(loaderDiv); 

    await fetch("https://fakestoreapi.com/products")
        .then((res) => res.json())
        .then((products) => {
            if (products.length !== 0){
                loaderDiv.style.display = "none"; 
                for (let product of products) {
                    createElement(product);
                }
            }
            else {
                throw new Error('No Product Found.'); 
            }
            
        })
        .catch(err => {
            loaderDiv.style.display = "none"; 
            let errorEl = document.createElement("p");
            errorEl.innerHTML = `<i class="fa-solid fa-xmark cross"></i> ${err}`;
            errorEl.classList.add("error"); 
            containerEl.append(errorEl); 
        })
}


function init(){
    let username = sessionStorage.getItem('name');
    if(username !== null) {
        userGreetingEl.innerHTML = `Hello, <span class='name'>${username.toLowerCase()}</span><i class="fa-solid fa-user-astronaut user-icon"></i> `; 
        fetchProducts();     
    }  
    else {
        let errorEl = document.createElement('p'); 
        errorEl.innerHTML = `<i class="fa-solid fa-xmark cross"></i> Please create an account <a href="./signup.html">here</a>.`; 
        errorEl.classList.add('error'); 
        containerEl.append(errorEl); 
    }
}

init(); 